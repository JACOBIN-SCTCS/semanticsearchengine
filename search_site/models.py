from django.db import models
from django.contrib.auth.models import User

DOC_CATEGORIES_CHOICES = [
    ("MINUTES", "MINUTES"),
    ("TECHDOCS", "Technical Documents"),

]


class DocCategories(models.Model):
    id = models.AutoField(primary_key= True)
    doc_category_name = models.CharField(max_length = 15, default="MINUTES", choices = DOC_CATEGORIES_CHOICES)

    def __str__(self):
        return self.doc_category_name


class DocData(models.Model):

    id = models.AutoField(primary_key=True)
    doc_name = models.CharField(max_length = 30)
    doc_start = models.IntegerField(default = 0)
    doc_end  = models.IntegerField(default = 0)
    doc_content = models.TextField()
    doc_vector_index = models.IntegerField(default = 0)
    doc_added_by = models.ForeignKey(User, blank = True, null = True, on_delete=models.SET_NULL)
    doc_file = models.FileField(upload_to='docs/')
    doc_categories = models.ManyToManyField(DocCategories)
    added_by_username = models.CharField(default="",max_length=20)
    doc_active = models.IntegerField(default=1)

    def __str__(self):
        return "Document No: " + str(self.id  )
    

class DocHueyTracker(models.Model):
    id = models.AutoField(primary_key=True)
    task_id = models.CharField(max_length=100)
    doc_id = models.ForeignKey(DocData, on_delete=models.CASCADE)
    doc_upload_status = models.CharField(max_length=100)
    doc_upload_status_active = models.IntegerField(default=1)


