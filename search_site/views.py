from django.shortcuts import render
from django.http import HttpResponse
from paddleocr import PaddleOCR
from .forms import QueryForm, FileUploadForm
from django.shortcuts import redirect
from .models import DocCategories, DocData, DocHueyTracker
import os
from sentence_transformers import SentenceTransformer,util
import numpy as np
import sys, fitz
import pickle
import string
import re
from django.conf import settings
from django.shortcuts import redirect , HttpResponseRedirect
import os
from .tasks import add_document_to_index
from .forms import LoginForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate,login,logout
import chromadb
from .tasks import CHROMADB_FOLDER_NAME, COLLECTION_NAME
import cv2
import fitz
from PIL import Image
import io
# Create your views here.

LOGIN_URL = "/login"
DASHBOARD_URL = "/dashboard"
UPLOAD_URL = "/upload"
HOME_PAGE_URL = "/"

def compute_query_vector(query):
    modelspath = os.path.join(settings.MEDIA_ROOT,"sentence_transformer_model")
    sbert_model = SentenceTransformer('all-mpnet-base-v2',cache_folder=modelspath)
    query_vec = sbert_model.encode([query])[0]
    return query_vec

def softmax(x):
    return(np.exp(x)/np.exp(x).sum())


def login_user(request):
    
    if request.method == "POST":
        login_form = LoginForm(request.POST)
        username = request.POST["username"]
        password = request.POST["password"]

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request,user)
            return HttpResponseRedirect(UPLOAD_URL)
    
    else:
        login_form = LoginForm()
    return render(request,"search_site/login.html",{"login_form" : login_form})

@login_required()
def uploader_dashboard(request):
    return render(request,"search_site/uploader_dashboard.html",{})


def index(request):

    results = DocData.objects.none()
    
    if request.method == "POST":
        query_form = QueryForm(request.POST)
        if query_form.is_valid():
            cleaned_query = query_form.cleaned_data['search_text']
    
            # Cleaned query would be the input from the user do the remaining
            query_vec = compute_query_vector(cleaned_query)
            
            chroma_client = chromadb.PersistentClient(path=os.path.join(settings.BASE_DIR,CHROMADB_FOLDER_NAME))
            collection = chroma_client.get_or_create_collection(COLLECTION_NAME)


            query_result = collection.query(
                query_embeddings=[query_vec.tolist()],
                n_results=5,
                #where={"metadata_field": "is_equal_to_this"},
                #where_document={"$contains":"search_string"}
            )
            print(query_result)

            document_metadata = query_result['metadatas'][0]
            retrieved_documents_set = set()
            for i in range(len(document_metadata)):
                retrieved_documents_set.add(document_metadata[i]['doc_id'])

            results = DocData.objects.filter(id__in=list(retrieved_documents_set))
           
    else:
        query_form = QueryForm()
                          
    return render(request,"search_site/base.html",{'query_form' : query_form, 'results' : results, 'pages' : [1,2]})

def highlightpdf(request):  
    if request.GET:
        query_parameters = request.GET
        doc_id = int(query_parameters['doc'])
        pages = query_parameters['pages'].strip('][').split(',')
        search_text = query_parameters['search_text']
        print(doc_id)
        print(pages)
        print(search_text)
        
        doc = DocData.objects.get(id=doc_id)
        ocr = PaddleOCR(use_angle_cls=True, lang='en')
        pdf_document = fitz.open(doc.doc_file.path)
        images = []
        for page in pdf_document:
            
            if(page.number in pages):
                filepath = doc.doc_file.path
                file_name="".join(filepath.split("\\")[-1].split(".")[:-1])
                doc_images_folder = os.path.join(settings.MEDIA_ROOT,"docs")
                page_file_name = "page_"  + str(page.number) + "_" + os.path.basename(file_name) + ".png"
                page_destination = os.path.join(doc_images_folder,page_file_name)
                result = ocr.ocr(page_destination, cls=True)
                img = cv2.imread(page_destination)

                for line in result:
                    for word_info in line:
                        bbox, (sentence, confidence) = word_info
                        words = sentence.split()
                        for i, word in enumerate(words):
                            print(f'Word: {word}, Confidence: {confidence}')

                            if word.lower() in set(search_text.lower().split()):
                                found = True
                                print(word)
                                # Get the bounding box coordinates
                                bbox = word_info[0]
                                print(f'Word "{search_text}" found at coordinates: {bbox}')
                                # Optionally, draw the bounding box on the image
                                cv2.polylines(img, [np.array(bbox, dtype=np.int32)], isClosed=True, color=(0, 255, 0), thickness=2)
                            else:
                                print("Invalid Input")
                                results = None    
                images.append(Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB)))
            else:
                pix = page.get_pixmap(matrix=fitz.Matrix(2.0, 2.0))
                data = pix.tobytes()
                img_bytes =  Image.open(io.BytesIO(data))
                images.append(img_bytes)
       
        images[0].save("all-my-pics.pdf", append_images = images[1:])
        #doc.save("all-my-pics.pdf")

        #return len(images)
        #return images
   

        #print(doc[0].doc_content)
        #ocr = PaddleOCR(use_angle_cls=True, lang='en')
        #pagenumber = 0
        #for doc in results:

    else:
        return HttpResponse("Testing")

# NOT USED ANYMORE
def handle_uploaded_file(f):
    file_dir = os.path.join(settings.MEDIA_ROOT,'docs')
    file_path = os.path.join(file_dir,f.name)
    with open(file_path, "wb+") as destination:
        for chunk in f.chunks():
            destination.write(chunk)
@login_required()
def upload_file(request):
    if request.POST:
        upload_form = FileUploadForm(request.POST, request.FILES)
        if upload_form.is_valid():
            print("REACHED HERE INSIDE VALID")
            if request.user == None:
                return HttpResponseRedirect(LOGIN_URL)
            
            file_data = request.FILES['doc_file']
            logged_user = request.user
            form_data = upload_form.cleaned_data
            
            new_doc = DocData(
                        doc_name=file_data.name,
                        doc_content = "",
                        doc_start = form_data['doc_start'],
                        doc_end = form_data['doc_end'],
                        doc_vector_index = 0,
                        doc_file = file_data,
                        doc_added_by = logged_user,
                        added_by_username = logged_user.username
                      )
    
            new_doc.save()
            doc_categories = DocCategories.objects.filter(doc_category_name__in = form_data['doc_categories']).values('id')
            for doc_category in doc_categories:
                new_doc.doc_categories.add(doc_category['id'])
            new_doc.save()
            
            
            task = add_document_to_index(doc_id=new_doc.id,form_data=upload_form.cleaned_data)
            task_status = DocHueyTracker.objects.create(task_id=task.id,doc_id = new_doc,doc_upload_status = "STARTED")
            task_status.save()
            return HttpResponse("File uploaded successfully")
    else:
        upload_form = FileUploadForm()
    return render(request,"search_site/file_upload.html",{'upload_form': upload_form})

def logout_user(request):
    logout(request)
    return HttpResponseRedirect(HOME_PAGE_URL)

