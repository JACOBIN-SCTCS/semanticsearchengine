from django import forms
from .models import DOC_CATEGORIES_CHOICES


class LoginForm(forms.Form):
    username = forms.CharField(label="Username")
    password = forms.CharField(label="Password", widget= forms.PasswordInput())

class QueryForm(forms.Form):
    search_text = forms.CharField(label="Enter the input to search")


class FileUploadForm(forms.Form):
    
    OPTIONS = DOC_CATEGORIES_CHOICES
    doc_start = forms.IntegerField(label="Starting page to extract contents" , initial=0, required=True)
    doc_end = forms.IntegerField(label="Ending page to extract contents", initial= 0 , required=False)
    doc_file = forms.FileField()

    doc_categories = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple,
                                          choices=OPTIONS)