from huey.contrib.djhuey import periodic_task, task
from huey.contrib.djhuey import signal
from huey.contrib.djhuey import db_periodic_task, db_task
from .models import DocData , DocCategories, DocHueyTracker
from django.conf import settings
import os
import pickle
import fitz
import pytesseract as tess
from PIL import Image
from sentence_transformers import SentenceTransformer,util
import numpy as np
import chromadb
from paddleocr import PaddleOCR

from huey import signals
from huey.signals import SIGNAL_COMPLETE, SIGNAL_ERROR,SIGNAL_EXECUTING, SIGNAL_LOCKED, SIGNAL_CANCELED,SIGNAL_REVOKED

CHROMADB_FOLDER_NAME = 'vector_database'
COLLECTION_NAME = 'docvectordatabase'
tess.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract'
OCR_SCALE_X = 2.0
OCR_SCALE_Y = 2.0

DOC_METADATA_TEMPLATE = {
    "doc_id" : -1,
    "page_id" : -1
}



def compute_document_vector(file_content):
    model_file_path = os.path.join(settings.MEDIA_ROOT,"sentence_transformer_model")
    sbert_model = SentenceTransformer('all-mpnet-base-v2',cache_folder=model_file_path)
    res_vector = sbert_model.encode([file_content])[0]
    return res_vector


def read_pdf_pymupdf(filepath, start_page = 1, end_page = 1):
  file_name="".join(filepath.split("\\")[-1].split(".")[:-1])
  doc_images_folder = os.path.join(settings.MEDIA_ROOT,"docs")
  doc_images_folder = os.path.join(doc_images_folder,file_name)

  doc = fitz.open(filepath)  
  if(not os.path.exists(doc_images_folder)):
      os.makedirs(doc_images_folder)
  ocr = PaddleOCR(use_angle_cls=True, lang='en')    
  pages = []   
  try:
    for page in doc:  
      
      if(not (page.number >= start_page and page.number <= end_page)):
         continue
      zoom_x = OCR_SCALE_X  # horizontal zoom
      zoom_y = OCR_SCALE_Y  # vertical zoom
      mat = fitz.Matrix(zoom_x, zoom_y)
      pix = page.get_pixmap(matrix = mat)
      
      page_file_name = "page_"  + str(page.number) + "_" + os.path.basename(file_name) + ".png"
      page_destination = os.path.join(doc_images_folder,page_file_name)

      pix.save(page_destination)
      result = ocr.ocr(page_destination, cls=True)
      
      sentences = []
      for line in result:
        for word_info in line:
          _, (sentence, confidence) = word_info
          sentences.append(sentence)

      #text = tess.image_to_string(Image.open(page_destination))
      pages.append("".join(sentences))

  except Exception as e :
    print(str(e))
  
  return pages
  # COMMENTED OUT
  #return "\n".join(pages)


@db_task()
def add_document_to_index(doc_id,form_data):
    chroma_client = chromadb.PersistentClient(path= os.path.join(settings.BASE_DIR,CHROMADB_FOLDER_NAME))
    collection =  chroma_client.get_or_create_collection(name=COLLECTION_NAME)

    new_doc = DocData.objects.get(id = doc_id, doc_active=1)
    doc_content = read_pdf_pymupdf(new_doc.doc_file.path, form_data['doc_start'] , form_data['doc_end'])

    doc_content_string = "\n".join(doc_content)
    new_doc.doc_content = doc_content_string
    
    # NEWLY ADDED CODE FOR INCORPORATING CHROMA DB
    vectors_to_add = []
    vectors_metadata = []
    vectors_ids = []

    for i in  range(len(doc_content)):
       page  =  doc_content[i]
       page_vector = compute_document_vector(page)
       vectors_to_add.append(page_vector.tolist())
       
       metadata_copy = DOC_METADATA_TEMPLATE.copy()
       metadata_copy["doc_id"] = new_doc.id
       metadata_copy["page_id"] = i
       vectors_metadata.append(metadata_copy)

       vector_id = str(metadata_copy["doc_id"]) + "_" + str(metadata_copy["page_id"])
       vectors_ids.append(vector_id)

    collection.add(
       embeddings= vectors_to_add,
       metadatas= vectors_metadata,
       documents= doc_content,
       ids= vectors_ids
    )

    new_doc.save()


@signal(SIGNAL_ERROR, SIGNAL_EXECUTING, SIGNAL_LOCKED, SIGNAL_CANCELED, SIGNAL_REVOKED)
def task_not_executed_handler(signal, task, exc=None):
    # This handler will be called for the 4 signals listed, which
    # correspond to error conditions.
    print('[%s] %s - not executed' % (signal, task.id))
    doc_processing_status = DocHueyTracker.objects.filter(task_id = task.id,doc_upload_status_active=1)[0]
    doc_processing_status.doc_upload_status = "FAILED"
    doc_processing_status.save()
    
    

@signal(SIGNAL_COMPLETE)
def task_success(signal, task):
    # This handle will be called for each task that completes successfully.
    doc_processing_status = DocHueyTracker.objects.filter(task_id = task.id,doc_upload_status_active=1)[0]
    doc_processing_status.doc_upload_status = "SUCCEED"
    doc_processing_status.save()    