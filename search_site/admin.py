from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import DocCategories, DocData, DocHueyTracker
#from .models import SearchSiteUser

# class CustomUserAdmin(UserAdmin):
#     pass

# admin.site.register(SearchSiteUser, CustomUserAdmin)
# Register your models here.

admin.site.register(DocData)
admin.site.register(DocCategories)
admin.site.register(DocHueyTracker)