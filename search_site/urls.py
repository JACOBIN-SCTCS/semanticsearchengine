from django.urls import path

from . import views

app_name = 'search_site'
urlpatterns = [
    path("", views.index, name="index"),
    path("upload/",views.upload_file,name="upload"),
    path("dashboard",views.uploader_dashboard, name="uploader_dashboard"),
    path("highlight", views.highlightpdf, name="highlight_pdf"),
    path("login",views.login_user, name="login"),
    path("logout", views.logout_user,name="logout")
]